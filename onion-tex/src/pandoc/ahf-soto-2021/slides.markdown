---
title: Updates from the Network Team
date: November 17, 2021
institute: State of the Onion
author:
  - name: Alexander Færøy
    email: ahf@torproject.org
slides:
    aspect-ratio: 169
    font-size: 14pt
    table-of-contents: false
---

## Network Team Summary of 2021

- Support the new \alert{Network Health Team} on different projects.
- Support the \alert{Anti-censorship Team} on Bridge and Pluggable Transport
  stability and usability issues in Tor.
- Work with friends that are integrating Tor into their applications.
- Ramping down on new client features in the C implementation of Tor.
- Ramping up on Rust development as part of the Arti project.

## UDP Support in the Tor Network

Support UDP for Tor clients and Exit nodes to allow support for modern internet
applications such as: Crypto wallets, streaming, VoIP, and \emph{hopefully}
WebRTC-based applications.

Client and Exit nodes will require upgrades for deployment.

Use Tor's Congestion Control system to decide when to drop packets at the edges.

Specification work being tracked in
\href{https://gitlab.torproject.org/tpo/core/torspec/-/issues/73}{torspec\#73}
on \href{https://gitlab.torproject.org/}{Tor's Gitlab}.

## UDP Support in the Tor Network

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[line width=4.0pt] (r1) edge (r2);
    \path[ultra thick, dashed, color=green] (r1) edge (r2);

    \path[line width=4.0pt] (r2) edge (r7);
    \path[ultra thick, dashed, color=green] (r2) edge (r7);

    %% Path between Alice and R1.
    \path[line width=4.0pt] (-4.4, -0.4) edge (r1);
    \path[ultra thick, dashed, color=green] (-4.4, -0.4) edge (r1);

    \path[ultra thick, dashed, color=green] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## Conflux

The goal is to overcome Tor's network performance bottlenecks using \textbf{Traffic Splitting}.

Current work specified by David Goulet and Mike Perry in Tor's
\href{https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/329-traffic-splitting.txt}{Proposal
\#329}.

Based on work by Mashael AlSabah, Kevin Bauer, Tariq Elahi, and Ian Goldberg in
the paper \href{https://www.freehaven.net/anonbib/papers/pets2013/paper_65.pdf}{The Path
Less Travelled: Overcoming Tor’s Bottlenecks with Traffic Splitting}.

Prototyping and Shadow experimentation will begin in \textbf{early 2022}.

## Conflux {.t}

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r7);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    \path[thick] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## Conflux {.t}

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r7);
    \path[thick] (r5) edge (r6);
    \path[thick] (r6) edge (r7);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between Alice and R5
    \path[thick] (-4.4, -0.4) edge (r5);

    \path[thick] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## Conflux

Thanks to \textbf{Per Hurtig} for helping us with the framing of the MultiPath
TCP (MPTCP) problem space.

Thanks to \textbf{Simone Ferlin} for clarifications on the
\href{https://olivier.mehani.name/publications/2016ferlin_blest_blocking_estimation_mptcp_scheduler.pdf}{BLEST
paper}, and for pointing us at the Linux kernel implementation.

Extreme thanks goes to \textbf{Toke Høiland-Jørgensen}, who helped immensely
towards our understanding of how the BLEST condition relates to edge connection
pushback, and for clearing up many other misconceptions we had.

Finally, thanks to \textbf{Mashael AlSabah}, \textbf{Kevin Bauer},
\textbf{Tariq Elahi}, and \textbf{Ian Goldberg}, for the original Conflux
paper.

## {.c .plain .noframenumbering}

\centering

\Large Next up is \textbf{Nick Mathewson} who will give you an update on the
\textbf{Arti} project.

\vfill

\normalsize

\begin{columns}
    \begin{column}{0.60\textwidth}
        \begin{itemize}
            \setlength\itemsep{0.75em}

            \item[\faTwitter] \colorhref{https://twitter.com/ahfaeroey}{@ahfaeroey}
            \item[\faEnvelope] \colorhref{mailto:ahf@torproject.org}{ahf@torproject.org}
            \item[\faKey] OpenPGP: \\ \texttt{1C1B C007 A9F6 07AA 8152} \\ \texttt{C040 BEA7 B180 B149 1921}
        \end{itemize}
    \end{column}

    \begin{column}{0.40\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/tor_bike.png}
        \end{center}
    \end{column}
\end{columns}

## {.c .plain .noframenumbering}

\centering

This work is licensed under a

\large \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons \\ Attribution-ShareAlike 4.0 International License}

\vfill

\ccbysa

\vfill
