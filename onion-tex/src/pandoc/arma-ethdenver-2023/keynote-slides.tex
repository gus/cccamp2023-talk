%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

\usetheme{onion}

\title{Lessons from the Tor community}
%\title{Lessons from communications metadata security}

\date{March 4, 2023}
\author{Roger Dingledine}
\institute{The Tor Project}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

%%%%%%% (1) Intro to Tor

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 30ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

\begin{frame}[t]{Threat Model}
\protect\hypertarget{threat-model}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

\large\{What can the attacker do?\}
\end{frame}

\begin{frame}[t]{Threat Model}
\protect\hypertarget{threat-model-1}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Adversary.
    \node[maninblack, monitor, minimum size=1.3cm] (adversary)at (-5, -2) {};

    %% Adversary wiretap.
    \node[wiretap] (wiretap) at (-3.4, -0.15) {};

    %% Adversary path.
    \path[thick, red!40] (adversary) edge (wiretap);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Threat Model}
\protect\hypertarget{threat-model-2}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay, fill=red!40, text=black] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay, fill=red!40, text=black] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Adversary.
    \node[maninblack, monitor, minimum size=1.3cm] (adversary) at (-5, -2) {};

    %% Path between our adversary and R1 and R3.
    \path[thick, red!40] (adversary) edge (r1);
    \path[thick, red!40, bend right] (-3.5, -3.0) edge (r3);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Threat Model}
\protect\hypertarget{threat-model-3}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Adversary.
    \node[maninblack, monitor, minimum size=1.3cm] (adversary) at (-5, -2) {};

    %% Adversary wiretap.
    \node[wiretap] (wiretap) at (3.4, -0.38) {};

    %% Path between our adversary and its wiretap.
    \path[thick, red!40, bend right] (-3.5, -3.0) edge (wiretap);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Threat Model}
\protect\hypertarget{threat-model-4}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[maninblack, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Anonymity isn't Encryption}
\protect\hypertarget{anonymity-isnt-encryption}{}
\centering
\begin{tikzpicture}
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% Encrypted channel.
    \draw[<->,thick, OnionDarkPurple!80] (-3.5, 0) -- (3.5, 0);

    %% Encrypted data.
    \node[] at (0, 0.5) {\tiny{\texttt{\ldots RG9uJ3QgdXNlIGJhc2U2NCBmb3IgZW5jcnlwdGlvbi4\ldots}}};

    %% Adversary.
    \node[maninblack, monitor, minimum size=1.3cm] (adversary) at (-5, -2) {};
    \node[ellipse callout, draw, yshift=0.3cm, callout absolute pointer={(adversary.mouth)}, font=\scriptsize, align=center, fill=white] at (-2.7, -2.5) {Gibberish!};

    %% Adversary wiretap.
    \node[wiretap] (wiretap) at (-0.5, 0) {};

    %% Adversary path.
    \path[thick, red!40] (adversary) edge (wiretap);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\large Encryption just protects contents.

\end{frame}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{"We Kill People Based on Metadata."}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA.}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/bill-barr.png}
\end{figure}
\end{frame}

\begin{frame}[t]{Anonymity is different for each use case}
\protect\hypertarget{anonymity-privacy-security-1}{}
\centering
\begin{tikzpicture}

    %% Our speech bubble
    \tikzstyle{speech bubble}=[ellipse callout, scale=0.85, draw, yshift=0.3cm,
font=\scriptsize, align=center]

    %% The title above each person.
    \tikzstyle{title}=[font=\footnotesize\bfseries]

    %% Our arrow.
    \tikzstyle{arrow}=[->, thick, OnionDarkPurple!80, shorten >= 0.2cm, shorten <= 0.2cm]

    %% Anonymity in the center.
    \node[font=\bfseries] (anonymity) at (0, -0.5) {Anonymity};

    %% Physician -- our privacy concerned citizen.
    \node[title] at (-3.5, -1.0) {Private Citizens};
    \node[physician, mirrored, female, minimum size=1.0cm] (physician) at (-3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(physician.mouth)}] at (-5.5, -2.0) {It's privacy!};

    %% Arrows.
    \draw[arrow] (anonymity) -- (physician);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Anonymity is different for each use case}
\protect\hypertarget{anonymity-privacy-security-2}{}
\centering
\begin{tikzpicture}

    %% Our speech bubble
    \tikzstyle{speech bubble}=[ellipse callout, scale=0.85, draw, yshift=0.3cm,
font=\scriptsize, align=center]

    %% The title above each person.
    \tikzstyle{title}=[font=\footnotesize\bfseries]

    %% Our arrow.
    \tikzstyle{arrow}=[->, thick, OnionDarkPurple!80, shorten >= 0.2cm, shorten <= 0.2cm]

    %% Anonymity in the center.
    \node[font=\bfseries] (anonymity) at (0, -0.5) {Anonymity};

    %% Physician -- our privacy concerned citizen.
    \node[title] at (-3.5, -1.0) {Private Citizens};
    \node[physician, mirrored, female, minimum size=1.0cm] (physician) at (-3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(physician.mouth)}] at (-5.5, -2.0) {It's privacy!};

    %% Business man -- our business man who cares about security.
    \node[title] at (3.5, -1.0) {Businesses};
    \node[businessman, minimum size=1.0cm] (businessman) at (3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(businessman.mouth)}] at (5.5, -2.0) {It's network \\ security!};

    %% Arrows.
    \draw[arrow] (anonymity) -- (physician);
    \draw[arrow] (anonymity) -- (businessman);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Anonymity is different for each use case}
\protect\hypertarget{anonymity-privacy-security-3}{}
\centering
\begin{tikzpicture}

    %% Our speech bubble
    \tikzstyle{speech bubble}=[ellipse callout, scale=0.85, draw, yshift=0.3cm,
font=\scriptsize, align=center]

    %% The title above each person.
    \tikzstyle{title}=[font=\footnotesize\bfseries]

    %% Our arrow.
    \tikzstyle{arrow}=[->, thick, OnionDarkPurple!80, shorten >= 0.2cm, shorten <= 0.2cm]

    %% Anonymity in the center.
    \node[font=\bfseries] (anonymity) at (0, -0.5) {Anonymity};

    %% Man in black -- our government security person.
    \node[title] at (-3.5, 2.0) {Governments};
    \node[maninblack, mirrored, minimum size=1.0cm] (maninblack) at (-3.5, 1.0) {}; 
    \node[speech bubble, callout absolute pointer={(maninblack.mouth)}] at (-5.5, 1.0) {It's traffic-analysis \\ resistance!};

    %% Physician -- our privacy concerned citizen.
    \node[title] at (-3.5, -1.0) {Private Citizens};
    \node[physician, mirrored, female, minimum size=1.0cm] (physician) at (-3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(physician.mouth)}] at (-5.5, -2.0) {It's privacy!};

    %% Business man -- our business man who cares about security.
    \node[title] at (3.5, -1.0) {Businesses};
    \node[businessman, minimum size=1.0cm] (businessman) at (3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(businessman.mouth)}] at (5.5, -2.0) {It's network \\ security!};

    %% Arrows.
    \draw[arrow] (anonymity) -- (maninblack);
    \draw[arrow] (anonymity) -- (physician);
    \draw[arrow] (anonymity) -- (businessman);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{Anonymity is different for each use case}
\protect\hypertarget{anonymity-privacy-security-4}{}
\centering
\begin{tikzpicture}

    %% Our speech bubble
    \tikzstyle{speech bubble}=[ellipse callout, scale=0.85, draw, yshift=0.3cm,
font=\scriptsize, align=center]

    %% The title above each person.
    \tikzstyle{title}=[font=\footnotesize\bfseries]

    %% Our arrow.
    \tikzstyle{arrow}=[->, thick, OnionDarkPurple!80, shorten >= 0.2cm, shorten <= 0.2cm]

    %% Anonymity in the center.
    \node[font=\bfseries] (anonymity) at (0, -0.5) {Anonymity};

    %% Man in black -- our government security person.
    \node[title] at (-3.5, 2.0) {Governments};
    \node[maninblack, mirrored, minimum size=1.0cm] (maninblack) at (-3.5, 1.0) {}; 
    \node[speech bubble, callout absolute pointer={(maninblack.mouth)}] at (-5.5, 1.0) {It's traffic-analysis \\ resistance!};

    %% Alice -- our human rights activist.
    \node[title] at (3.5, 2.0) {Human Rights Activists};
    \node[alice, minimum size=1.0cm] (alice) at (3.5, 1.0) {};
    \node[speech bubble, callout absolute pointer={(alice.mouth)}] at (5.5, 1.0) {It's reachability!};

    %% Physician -- our privacy concerned citizen.
    \node[title] at (-3.5, -1.0) {Private Citizens};
    \node[physician, mirrored, female, minimum size=1.0cm] (physician) at (-3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(physician.mouth)}] at (-5.5, -2.0) {It's privacy!};

    %% Business man -- our business man who cares about security.
    \node[title] at (3.5, -1.0) {Businesses};
    \node[businessman, minimum size=1.0cm] (businessman) at (3.5, -2.0) {};
    \node[speech bubble, callout absolute pointer={(businessman.mouth)}] at (5.5, -2.0) {It's network \\ security!};

    %% Arrows.
    \draw[arrow] (anonymity) -- (maninblack);
    \draw[arrow] (anonymity) -- (alice);
    \draw[arrow] (anonymity) -- (physician);
    \draw[arrow] (anonymity) -- (businessman);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{A Simple Design}
\protect\hypertarget{a-simple-design}{}
\centering
\begin{tikzpicture}
    %% Define our styles.
    \tikzstyle{person}=[monitor, minimum size=1.0cm]
    \tikzstyle{arrow}=[->, thick, shorten >= 0.6cm, shorten <= 0.6cm]
    \tikzstyle{message}=[sloped, above, midway, font=\scriptsize]

    %% Our central relay.
    \node[circle, draw, thin, fill=OnionDarkPurple!80, text=white, minimum size=2.1cm, align=center] (relay) at (0, 0) {Relay};

    %% First couple.
    \node[alice, person] (alice_1) at (-4.5, 2.0) {};
    \node[bob, mirrored, person] (bob_3) at (4.5, -2.0) {};

    \draw[arrow, cyan!70] (alice_1) -- node [message] {$Enc(Bob_{3}, "x")$} (relay);
    \draw[arrow, cyan!70] (relay) -- node [message] {"x"} (bob_3);

    %% Second couple.
    \node[physician, female, person] (alice_2) at (-5.0, 0.0) {};
    \node[surgeon, mirrored, person] (bob_1) at (4.5, 2.0) {};

    \draw[arrow, purple!70] (alice_2) -- node [message]{$Enc(Bob_{1}, "y")$} (relay);
    \draw[arrow, purple!70] (relay) -- node [message] {"y"} (bob_1);

    %% Third couple.
    \node[police, female, person] (alice_3) at (-4.5, -2.0) {};
    \node[police, mirrored, person] (bob_2) at (5.0, 0.0) {};

    \draw[arrow, violet!70] (alice_3) -- node [message] {$Enc(Bob_{2}, "z")$} (relay);
    \draw[arrow, violet!70] (relay) -- node [message] {"z"} (bob_2);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Equivalent to most commercial proxy/VPN providers.
\end{frame}

\begin{frame}[t]{A Simple Design}
\protect\hypertarget{a-simple-design-1}{}
\centering
\begin{tikzpicture}
    %% Define our styles.
    \tikzstyle{person}=[monitor, minimum size=1.0cm]
    \tikzstyle{arrow}=[->, thick, shorten >= 0.6cm, shorten <= 0.6cm]
    \tikzstyle{message}=[sloped, above, midway, font=\scriptsize]

    %% Our central evil relay.
    \node[circle, draw, thin, fill=red!40, text=black, minimum size=2.1cm, align=center] (relay) at (0, 0) {Evil \\ Relay?};

    %% First couple.
    \node[alice, person] (alice_1) at (-4.5, 2.0) {};
    \node[bob, mirrored, person] (bob_3) at (4.5, -2.0) {};

    \draw[arrow, cyan!70] (alice_1) -- node [message] {$Enc(Bob_{3}, "x")$} (relay);
    \draw[arrow, cyan!70] (relay) -- node [message] {"x"} (bob_3);

    %% Second couple.
    \node[physician, female, person] (alice_2) at (-5.0, 0.0) {};
    \node[surgeon, mirrored, person] (bob_1) at (4.5, 2.0) {};

    \draw[arrow, purple!70] (alice_2) -- node [message] {$Enc(Bob_{1}, "y")$} (relay);
    \draw[arrow, purple!70] (relay) -- node [message] {"y"} (bob_1);

    %% Third couple.
    \node[police, female, person] (alice_3) at (-4.5, -2.0) {};
    \node[police, mirrored, person] (bob_2) at (5.0, 0.0) {};

    \draw[arrow, violet!70] (alice_3) -- node [message] {$Enc(Bob_{2}, "z")$} (relay);
    \draw[arrow, violet!70] (relay) -- node [message] {"z"} (bob_2);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}
\end{frame}

\begin{frame}[t]{A Simple Design}
\protect\hypertarget{a-simple-design-2}{}
\centering
\begin{tikzpicture}
    %% Define our styles.
    \tikzstyle{person}=[monitor, minimum size=1.0cm]
    \tikzstyle{arrow}=[->, thick, shorten >= 0.6cm, shorten <= 0.6cm]
    \tikzstyle{message}=[sloped, above, midway, font=\scriptsize]

    %% Our central relay.
    \node[circle, draw, thin, fill=OnionDarkPurple!80, text=white, minimum size=2.1cm, align=center] (relay) at (0, 0) {Relay};

    %% Our wiretap
    \node[circle, draw, thick, red!40, minimum size=2.8cm, align=center] (wiretap) at (0, 0) {};

    %% First couple.
    \node[alice, person] (alice_1) at (-4.5, 2.0) {};
    \node[bob, mirrored, person] (bob_3) at (4.5, -2.0) {};

    \draw[arrow, cyan!70] (alice_1) -- node [message] {$Enc(Bob_{3}, "x")$} (relay);
    \draw[arrow, cyan!70] (relay) -- node [message] {"x"} (bob_3);

    %% Second couple.
    \node[physician, female, person] (alice_2) at (-5.0, 0.0) {};
    \node[surgeon, mirrored, person] (bob_1) at (4.5, 2.0) {};

    \draw[arrow, purple!70] (alice_2) -- node [message] {$Enc(Bob_{1}, "y")$} (relay);
    \draw[arrow, purple!70] (relay) -- node [message] {"y"} (bob_1);

    %% Third couple.
    \node[police, female, person] (alice_3) at (-4.5, -2.0) {};
    \node[police, mirrored, person] (bob_2) at (5.0, 0.0) {};

    \draw[arrow, violet!70] (alice_3) -- node [message] {$Enc(Bob_{2}, "z")$} (relay);
    \draw[arrow, violet!70] (relay) -- node [message] {"z"} (bob_2);

    %% Adversary.
    \node[maninblack, person] (adversary) at (-2, -2) {};

    %% Adversary path.
    \path[thick, red!40] (adversary) edge (wiretap);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

%Timing analysis bridges all connections going through the relay.
Timing analysis lets an observer match up connections.
\end{frame}

\iffalse

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Paths between our three relays.
    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r3);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between R3 and Bob.
    \path[thick] (r3) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

%Add multiple relays so that no single relay can betray Alice.
Multiple relays so no single relay can link Alice to Bob.
\end{frame}

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design-1}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \node[] at (0, 2) {Anonymity Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Alice picks a path through the network: \(R_{1}\), \(R_{2}\), and
\(R_{3}\) before finally reaching Bob.
\end{frame}

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design-2}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    \pgfdeclarelayer{background}
    \pgfdeclarelayer{foreground}
    \pgfsetlayers{background,main,foreground}

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \begin{pgfonlayer}{background}
        \node[] at (0, 2) {Anonymity Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
    \end{pgfonlayer}

    %% The relay nodes inside the Anonymity Network cloud.
    \begin{pgfonlayer}{foreground}
        \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
    \end{pgfonlayer}

    %% Alice connects to R1.
    \path[line width=4.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Alice makes a session key with \(R_{1}\).
\end{frame}

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design-3}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    \pgfdeclarelayer{background}
    \pgfdeclarelayer{foreground}
    \pgfsetlayers{background,main,foreground}

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \begin{pgfonlayer}{background}
        \node[] at (0, 2) {Anonymity Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
    \end{pgfonlayer}

    %% The relay nodes inside the Anonymity Network cloud.
    \begin{pgfonlayer}{foreground}
        \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
    \end{pgfonlayer}

    %% Alice connects to R1.
    \path[line width=8.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
    \path[line width=4.0pt,draw=blue!80] (-4.2, -0.4) edge (r1.center);

    %% R1 connects to R2.
    \path[line width=4.0pt,draw=blue!80] (r1.center) edge (r2.center);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Alice asks \(R_{1}\) to extend to \(R_{2}\).
\end{frame}

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design-4}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    \pgfdeclarelayer{background}
    \pgfdeclarelayer{foreground}
    \pgfsetlayers{background,main,foreground}

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \begin{pgfonlayer}{background}
        \node[] at (0, 2) {Anonymity Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
    \end{pgfonlayer}

    %% The relay nodes inside the Anonymity Network cloud.
    \begin{pgfonlayer}{foreground}
        \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
    \end{pgfonlayer}

    %% Alice connects to R1.
    \path[line width=12.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
    \path[line width=8.0pt,draw=blue!80] (-4.2, -0.4) edge (r1.center);
    \path[line width=4.0pt,draw=yellow!80] (-4.2, -0.4) edge (r1.center);

    %% R1 connects to R2.
    \path[line width=8.0pt,draw=blue!80]   (r1.center) edge (r2.center);
    \path[line width=4.0pt,draw=yellow!80] (r1.center) edge (r2.center);

    %% R2 connects to R3.
    \path[line width=4.0pt,draw=yellow!80] (r2.center) edge (r3.center);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Alice asks \(R_{2}\) to extend to \(R_{3}\).
\end{frame}

\fi

\begin{frame}[t]{The Tor Design}
\protect\hypertarget{the-tor-design-5}{}
\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

    \pgfdeclarelayer{background}
    \pgfdeclarelayer{foreground}
    \pgfsetlayers{background,main,foreground}

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    %% The Anonymity Network cloud.
    \begin{pgfonlayer}{background}
        \node[] at (0, 2) {Anonymity Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
    \end{pgfonlayer}

    %% The relay nodes inside the Anonymity Network cloud.
    \begin{pgfonlayer}{foreground}
        \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
    \end{pgfonlayer}

    %% Alice connects to R1.
    \path[line width=12.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
    \path[line width=8.0pt,draw=blue!80]   (-4.2, -0.4) edge (r1.center);
    \path[line width=4.0pt,draw=yellow!80] (-4.2, -0.4) edge (r1.center);
    \path[thick,draw=black           ]     (-4.2, -0.4) edge (r1.center);

    %% R1 connects to R2.
    \path[line width=8.0pt,draw=blue!80]   (r1.center) edge (r2.center);
    \path[line width=4.0pt,draw=yellow!80] (r1.center) edge (r2.center);
    \path[thick,draw=black]                (r1.center) edge (r2.center);

    %% R2 connects to R3.
    \path[line width=4.0pt,draw=yellow!80] (r2.center) edge (r3.center);
    \path[thick,draw=black]                (r2.center) edge (r3.center);

    %% R3 connects to Bob.
    \path[thick,draw=black] (r3.center) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

Multiple relays so no single relay can link Alice to Bob.
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/bandwidth-2013-01-01-2023-03-04.png}
\end{figure}
\end{frame}

\begin{frame}{Tor's {\bf{safety}} comes from {\bf{diversity}}}

1. Diversity of relays. The more relays we have and the more diverse they
are, the fewer attackers are in a position to do traffic confirmation.
Research problem: How do we measure diversity over time?

2. Diversity of users and reasons to use it. 50000 users in Iran means
almost all of them are normal citizens.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/tor-browser-115.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/ddg-pbm.png}
\end{figure}
\end{frame}

\begin{frame}{Transparency for Tor is key}

\begin{itemize}[label=\textbullet]
\item<1-> Open source / free software
\item<1-> Public design documents and specifications
\item<1-> Publicly identified developers
\item<2-> Not a contradiction: privacy is about choice!
\end{itemize}

\end{frame}

\begin{frame}{Growing your community | sustainability}

Volunteer ecosystem, commons vs capitalism

Individual sustainability, keeping motivated long term

Funding tied into sustainability too: reproduce capitalism? get
govt funding? charge per user?

%relay diversity (network health) as we grow

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/ed-tor.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/ola-bini.png}
\end{figure}
\end{frame}

\begin{frame}{Technology is not neutral}

Power imbalances are central to Tor: it's more useful to
vulnerable people than those who already have power.

Technology is inherently political!

But that doesn't mean we need to like or endorse or talk about all of our
users. `Absolute free speech' vs `framing and community-building' vs
`how the system works technically'.

%how to approach bad users?
%``Tor is for everything no matter what'' is not sustainable and not smart. But, framing and community-building is
%  but there is a difference between how you build community and the
%  technical properties of your system

\end{frame}

\begin{frame}{The crypto wars}

We should take as many lessons as we can from the https win.

If you hear that NSA stores encrypted traffic for longer, what do you do?

``Jurisdictional arbitrage'', MLATs, CLOUD act

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/facebook_onion.png}
\includegraphics[width=\linewidth]{images/facebook_tor_news.png}
\end{figure}
\end{frame}

\begin{frame}{Censorship implies surveillance}

Recurring theme where western companies build the oppression tools

Censorship becoming more widespread in the west (e.g. RT.com in europe)

Iran's ``halal internet'' dreams, Russia heading there too

over-there-istan

\end{frame}

\begin{frame}{Center of privacy research}

Thriving interaction with research community

Attacking Tor is challenging so it's where the academics go

And we help mentor grad students, clarify and highlight research
questions, review papers, etc

\end{frame}

\iffalse

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/russia-sanctions.png}
\end{figure}
\end{frame}

\begin{frame}{First, a rant about sanctions}

Especially about hurting internet connectivity for people in Russia as
a way to punish their government.

Compare to the effects of Trump's ``maximum pressure'' sanctions
against Iran.

We will see the same outcome in Russia.

\end{frame}

\fi

\begin{frame}{Calls to action}

Understand the risks of centralized architectures

Fight the slippery slope that is internet censorship

Participate in privacy research, e.g. https://petsymposium.org/

Volunteer as a relay, a bridge, or a snowflake!

https://donate.torproject.org/

\end{frame}

\end{document}

